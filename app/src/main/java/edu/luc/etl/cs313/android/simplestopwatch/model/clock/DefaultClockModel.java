package edu.luc.etl.cs313.android.simplestopwatch.model.clock;

import java.util.Timer;
import java.util.TimerTask;

/**
 * An implementation of the internal clock.
 *
 * @author laufer
 */
public class DefaultClockModel implements ClockModel {

    // TODO make accurate by keeping track of partial seconds when canceled etc.

    private Timer timer;

    private int seconds;

    private OnTickListener listener;

    private OnPauseListener listener2;


    @Override
    public void setOnTickListener(final OnTickListener listener) {
        this.listener = listener;
    }

    @Override
    public void setOnPauseListener(final OnPauseListener listener2) {
        this.listener2 = listener2;
    }

    @Override
    public void start() {
        timer = new Timer();

        // The clock model runs onTick every 1000 milliseconds
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                // fire event
                listener.onTick();
            }
        }, /*initial delay*/ 1000, /*periodic delay*/ 1000);
    }

    @Override
    public void stop() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                listener2.onPause();
            }
        },0,99);
    }

    @Override
    public void timerStop() {
        timer.cancel();
    }


}
