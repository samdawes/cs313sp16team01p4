package edu.luc.etl.cs313.android.simplestopwatch.model.clock;

/**
 * Created by samdawes on 4/11/16.
 */
public interface OnPauseListener {
    void onPause();
}
