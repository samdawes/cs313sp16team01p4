package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import android.media.AudioManager;
import android.media.ToneGenerator;
import edu.luc.etl.cs313.android.simplestopwatch.R;

class AlarmState implements StopwatchState {

/* This alarm state is triggered when countdown state reaches one*/

    public AlarmState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionResetPause();
        sm.actionTimerStop();
        sm.actionInit();

    }

    // When the runtime reaches 0 the alarm will go off until the button is pressed and the app goes
    // to the initiated state
    @Override
    public void onPause() {
        final ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
        while (sm.actionGetRunTime() == 0){
        }
        {tg.startTone(ToneGenerator.TONE_CDMA_ALERT_NETWORK_LITE);}
    }

    // will not get used
    @Override
    public void onTick () {
    }

    @Override
    public void updateView () {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.ALARM;
    }

}