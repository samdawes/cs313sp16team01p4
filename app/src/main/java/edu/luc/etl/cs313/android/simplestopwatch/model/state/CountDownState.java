package edu.luc.etl.cs313.android.simplestopwatch.model.state;
import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import edu.luc.etl.cs313.android.simplestopwatch.R;

class CountDownState implements StopwatchState {

    public Activity activity;
    private Context context;
    private MediaPlayer mMediaPlayer;

    public CountDownState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }
    private final StopwatchSMStateView sm;
    // if onStartStop is pressed the timers will restart and it will go back to its initiated state.
    @Override
    public void onStartStop() {
        sm.actionTimerStop();
        sm.actionResetPause();
        sm.actionInit();
    }
    // When the run time reaches 0 the app will go to alarm state
    @Override
    public void onPause() {
        sm.actionDec();
        if (sm.actionGetRunTime() == 0){
            sm.toAlarmState();}
    }
    // will not get used
    @Override
    public void onTick () {
        sm.actionDec();
    }
    @Override
    public void updateView () {
        sm.updateUIRuntime();
    }
    @Override
    public int getId () {
        return R.string.COUNTDOWN;
    }
}