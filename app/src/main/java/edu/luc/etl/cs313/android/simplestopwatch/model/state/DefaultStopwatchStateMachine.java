package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simplestopwatch.model.time.TimeModel;

/**
 * An implementation of the state machine for the stopwatch.
 *
 * @author laufer
 */
public class DefaultStopwatchStateMachine implements StopwatchStateMachine {

    public DefaultStopwatchStateMachine(final TimeModel timeModel, final ClockModel clockModel) {
        this.timeModel = timeModel;
        this.clockModel = clockModel;
    }

    private final TimeModel timeModel;

    private final ClockModel clockModel;

    /**
     * The internal state of this adapter component. Required for the State pattern.
     */
    private StopwatchState state;

    protected void setState(final StopwatchState state) {
        this.state = state;
        uiUpdateListener.updateState(state.getId());
    }

    private StopwatchUIUpdateListener uiUpdateListener;

    @Override
    public void setUIUpdateListener(final StopwatchUIUpdateListener uiUpdateListener) {
        this.uiUpdateListener = uiUpdateListener;
    }

    //these are the states:
    // forward event uiUpdateListener methods to the current state
    // these must be synchronized because events can come from the
    // UI thread or the timer thread
    @Override public synchronized void onStartStop() { state.onStartStop(); }
    @Override public synchronized void onTick()      { state.onTick(); }
    @Override public synchronized void onPause() { state.onPause(); }

    @Override public void updateUIRuntime()     { uiUpdateListener.updateTime(timeModel.getRuntime()); }
    //@Override public void updateUIPausetime()   {uiUpdateListener.updateTime(timeModel.getPauseTime());}

    // States:
    private final StopwatchState STOPPED        = new StoppedState(this);
    private final StopwatchState RUNNING        = new RunningState(this);
    private final StopwatchState COUNTDOWN      = new CountDownState(this);
    private final StopwatchState ALARM          = new AlarmState(this);

    // transitions
    @Override public void toRunningState()      { setState(RUNNING); }
    @Override public void toStoppedState()      { setState(STOPPED); }
    @Override public void toCountdownState()    { setState(COUNTDOWN); }
    @Override public void toAlarmState()        { setState(ALARM); }

    // actions
    @Override public void actionInit()          { toStoppedState(); actionReset(); }
    @Override public void actionReset()         { timeModel.resetRuntime(); actionUpdateView(); }
    @Override public void actionStart()         { clockModel.start(); }
    @Override public void actionStop()          { clockModel.timerStop(); }
    @Override public void actionResetPause()    { timeModel.resetPauseTime(); }
    @Override public int actionGetPauseTime()   { return timeModel.getPauseTime(); }
    @Override public int actionGetRunTime()     { return timeModel.getRuntime(); }
    @Override public void actionTimerStop()     { clockModel.timerStop(); }
    @Override public void actionInc()           { timeModel.incRuntime(); actionUpdateView(); }
    @Override public void actionIncPauseTime()  { timeModel.incPauseTime(); }
    @Override public void actionUpdateView()    { state.updateView(); }
    @Override public void actionDec()           { timeModel.countDown(); actionUpdateView(); }
}
