package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class StoppedState implements StopwatchState {

    public StoppedState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    public int pauseTime;

    @Override
    public void onStartStop() {
        sm.actionStart();
        sm.actionInc();
//      sm.toRunningState();
        sm.actionResetPause();
        sm.actionStop();
    }

    @Override
    public void onPause() {

        while (sm.actionGetPauseTime() != 3) {
            sm.actionIncPauseTime();
        }

        if (sm.actionGetPauseTime() == 3) {
            sm.toCountdownState();
            sm.toRunningState();
        }

    }

    @Override
    public void onTick() {
        sm.actionInc();
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.STOPPED;
    }
}
