package edu.luc.etl.cs313.android.simplestopwatch.model.time;

import static edu.luc.etl.cs313.android.simplestopwatch.common.Constants.*;

/**
 * An implementation of the stopwatch data model.
 */
public class DefaultTimeModel implements TimeModel {

    private int runningTime = 0;

    private int pauseTime = 0;
    
    @Override
    public void resetPauseTime() { pauseTime = 0; }

    @Override
    public int getPauseTime() { return pauseTime; }

    @Override
    public void incPauseTime() {
        pauseTime++;
       // pauseTime = (pauseTime + SEC_PER_TICK);
    }

    @Override
    public void resetRuntime() {
        runningTime = 0;
    }

    @Override
    public void incRuntime() {

        runningTime = (runningTime + SEC_PER_TICK);
    }

    @Override
    public int getRuntime() {
        return runningTime;
    }

    @Override
    public void countDown() { runningTime = (runningTime - SEC_PER_TICK); }
}